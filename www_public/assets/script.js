document.querySelector("#repos .glass").appendChild(document.createElement("p")).textContent = "No repos found, try reloading the page. (errcode: 404)";
if(json != undefined){
 document.querySelector("#repos .glass").textContent = '';
 for (let i = 0; i < json.repoarray.length; i++) { 
  
  let myelement = document.querySelector("#repos .glass").appendChild(document.createElement("div"))
   myelement.setAttribute("onclick", `window.location='${json.repoarray[i].url}'`);
  myelement.classList.add("flat-glass");
   myelement.appendChild(document.createElement("p")).textContent = `Repo name: '${json.repoarray[i].name}', Version: ${json.repoarray[i].version}, Language: ${json.repoarray[i].language}, Platform ${json.repoarray[i].platform}`;
 }
 document.querySelector("#repos .glass").setAttribute("class", "shadow")
}